﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using FCamara.Domain.Model;
using FCamara.Domain.Repository;
using Moq;
using NUnit.Framework;

namespace FCamara.Application.Service.Tests
{
    [TestFixture]
    public class ProductServiceTest
    {
        private List<Product> _products;
        private IProductService _productService;
        private Mock<IProductRepository> _mockProductRepository;

        [SetUp]
        public void Init()
        {
            this._products = new List<Product>
            {
                new Product("Bola", "Bola de Futebol", 99.99m),
                new Product("Chuteira", "Chuteira de Futebol", 250.00m)
            };

            this._mockProductRepository = new Mock<IProductRepository>();
            this._mockProductRepository.Setup(x => x.GetAll()).Returns(_products);

            this._productService = new ProductService(this._mockProductRepository.Object);
        }

        [Test]
        public void Should_Return_List_Of_Products()
        {
            var products = this._productService.FindAllProducts().ToList();
            Assert.IsTrue(products.Any());
            Assert.AreEqual(2, products.Count());
            Assert.AreEqual("Bola", products[0].Name);

            this._mockProductRepository.Verify(mock => mock.GetAll(), Times.Once());
        }
    }
}