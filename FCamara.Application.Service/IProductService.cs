﻿using System.Collections.Generic;
using FCamara.Domain.Model;

namespace FCamara.Application.Service
{
    public interface IProductService
    {
        IEnumerable<Product> FindAllProducts();
    }
}