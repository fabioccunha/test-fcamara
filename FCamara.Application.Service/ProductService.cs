﻿using System.Collections.Generic;
using FCamara.Domain.Model;
using FCamara.Domain.Repository;

namespace FCamara.Application.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            this._productRepository = productRepository;
        }

        public IEnumerable<Product> FindAllProducts()
        {
            return this._productRepository.GetAll();
        }
    }
}