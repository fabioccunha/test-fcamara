﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCamara.Domain.Model;
using NUnit.Framework;

namespace FCamara.Domain.Tests
{
    [TestFixture]
    public class ProductTest
    {
        [Test]
        public void Create_New_Product()
        {
            var product = new Product("Bola", "Bola de Futebol Nike", 99.99m); 
            
            Assert.AreEqual(product.Name, "Bola");
            Assert.AreEqual(product.Description, "Bola de Futebol Nike");
            Assert.AreEqual(product.Price, 99.99m);
        }
    }
}
