﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCamara.Domain.Model;

namespace FCamara.Domain.Repository
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
