﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCamara.Domain.Model;

namespace FCamara.Domain.Repository
{
    public interface IRepository<TEntity>
    {
        TEntity GetById(int id);

        IEnumerable<TEntity> GetAll(); 

        void Add(TEntity entity);

        void Remove(TEntity entity);
    }
}
