﻿using System;
using FCamara.Infrastructure.Authorization.Models;
using FCamara.Infrastructure.Authorization.Validation;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FCamara.Infrastructure.Authorization.Tests
{
    [TestFixture]
    public class GuidTokenValidationTest
    {
        private GuidTokenValidation _guidTokenValidation;
        private ITokenStore _tokenStore;
        private GuidToken _validToken;

        [SetUp]
        public void Init()
        {
            this._tokenStore = new InMemoryTokenStore();
            this._guidTokenValidation = new GuidTokenValidation(this._tokenStore);
            this._validToken = new TokenFactory().GetToken("subject", "issuer", "claims");
        }

        [Test]
        public void Should_Validate_A_Valid_Token()
        {
            this._tokenStore.SetToken(this._validToken);

            GuidTokenValidationResult validationResult = this._guidTokenValidation.Validate(this._validToken.Value);

            Assert.IsNotNull(validationResult);
            Assert.IsTrue(validationResult.Valid);

            Assert.AreEqual(validationResult.Token.Value, this._validToken.Value);
            Assert.AreEqual(validationResult.Token.CreationDate, this._validToken.CreationDate);
        }

        [Test]
        public void Should_Return_InvalidResult_With_An_Invalid_Token()
        {
            this._tokenStore.SetToken(this._validToken);

            GuidTokenValidationResult validationResult = this._guidTokenValidation.Validate(Guid.NewGuid().ToString());

            Assert.IsNotNull(validationResult);
            Assert.IsFalse(validationResult.Valid);
            
            Assert.IsNull(validationResult.Token);
        }

    }
}