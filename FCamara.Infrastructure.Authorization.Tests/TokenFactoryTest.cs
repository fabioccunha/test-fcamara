﻿using FCamara.Infrastructure.Authorization.Models;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FCamara.Infrastructure.Authorization.Tests
{
    [TestFixture]
    public class TokenFactoryTest
    {
        [Test]
        public void Create_New_Guid_Token()
        {
            GuidToken token = new TokenFactory().GetToken("subject", "issuer", "claims");
            
            Assert.NotNull(token);
            Assert.IsNotEmpty(token.Value);
            Assert.NotNull(token.CreationDate);
        }
    }
}