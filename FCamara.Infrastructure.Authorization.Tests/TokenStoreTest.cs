﻿using System;
using FCamara.Infrastructure.Authorization.Models;
using NUnit.Framework;

namespace FCamara.Infrastructure.Authorization.Tests
{
    [TestFixture]
    public class TokenStoreTest
    {
        private ITokenStore _tokenStore;
        private GuidToken _token;

        [SetUp]
        public void Init()
        {
            this._tokenStore = new InMemoryTokenStore();
            this._token = new TokenFactory().GetToken("subject", "issuer", "claims");
        }

        [Test]
        public void Store_New_Token()
        {   
            Assert.DoesNotThrow(() => this._tokenStore.SetToken(this._token));
            GuidToken token = (GuidToken)this._tokenStore.GetToken(this._token.Value);

            Assert.NotNull(token);
        }

        [Test]
        public void Should_Throws_ArgumentNullException_When_Pass_NULL_On_SetToken()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => this._tokenStore.SetToken(null));
            Assert.AreEqual(exception.ParamName, "token");
        }

        [Test]
        public void Retrieve_A_Token_Passing_A_Valid_Token_Value()
        {
            this._tokenStore.SetToken(this._token);

            GuidToken token = (GuidToken)this._tokenStore.GetToken(this._token.Value);

            Assert.NotNull(token);
            Assert.IsNotEmpty(token.Value);
            Assert.AreEqual(this._token.Value, token.Value);
            Assert.NotNull(token.CreationDate);
        }

        [Test]
        public void GetToken_Should_Return_Null_When_Retrieve_An_Invalid_Token()
        {
            this._tokenStore.SetToken(this._token);

            var token = this._tokenStore.GetToken(Guid.NewGuid().ToString());
            Assert.IsNull(token);
        }
    }
}