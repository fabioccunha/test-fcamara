﻿using System;
using System.Runtime.Serialization;

namespace FCamara.Infrastructure.Authorization.Dto
{
    [DataContract]
    public class TokenResult
    {
        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public DateTime Expiration { get; set; }
    }
}