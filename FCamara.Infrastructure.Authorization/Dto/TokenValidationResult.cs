﻿using System.Runtime.Serialization;

namespace FCamara.Infrastructure.Authorization.Dto
{
    [DataContract]
    public class TokenValidationResult
    {
        [DataMember]
        public bool Valid { get; set; }
    }
}