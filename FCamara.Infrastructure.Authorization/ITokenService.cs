﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FCamara.Infrastructure.Authorization.Dto;

namespace FCamara.Infrastructure.Authorization
{
    [ServiceContract]
    public interface ITokenService
    {
        [OperationContract]
        TokenResult GetToken();

        [OperationContract]
        TokenValidationResult ValidateToken(string token);
    }
}
