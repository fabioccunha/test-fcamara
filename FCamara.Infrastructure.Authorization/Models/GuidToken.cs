﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCamara.Infrastructure.Authorization.Models
{
    public class GuidToken : IToken
    {
        public GuidToken()
        {
            this.Value = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now.AddMinutes(1);
        }
        
        public string Value { get; }

        public DateTime CreationDate { get; }
    }
}
