﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCamara.Infrastructure.Authorization.Models
{
    public interface IToken
    {
        string Value { get; }
    }
}
