﻿namespace FCamara.Infrastructure.Authorization.Models
{
    public interface ITokenStore
    {
        IToken GetToken(string tokenValue);

        void SetToken(IToken token);
    }
}