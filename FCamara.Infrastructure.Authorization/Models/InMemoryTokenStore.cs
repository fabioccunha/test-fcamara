﻿using System;
using System.Runtime.Caching;
using System.Runtime.Remoting.Lifetime;
using System.Web;

namespace FCamara.Infrastructure.Authorization.Models
{
    public class InMemoryTokenStore : ITokenStore
    {
        private static readonly ObjectCache Cache = MemoryCache.Default;
        private const int Lifetime = 60;

        public IToken GetToken(string tokenValue)
        {
            if (string.IsNullOrEmpty(tokenValue))
            {
                throw new ArgumentException("Token should not to be empty");
            }

            return (IToken)Cache.Get(tokenValue);
        }

        public void SetToken(IToken token)
        {
            if (token == null)
            {
                throw new ArgumentNullException(nameof(token));
            }

            Cache.Set(token.Value, token, GetConfigPolicy());
        }

        private CacheItemPolicy GetConfigPolicy()
        {
            var policy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(Lifetime)
            };
            return policy;
        }

    }
}
