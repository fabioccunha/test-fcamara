﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FCamara.Infrastructure.Authorization.Dto;
using FCamara.Infrastructure.Authorization.Models;
using FCamara.Infrastructure.Authorization.Validation;

namespace FCamara.Infrastructure.Authorization
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TokenService" in both code and config file together.
    public class TokenService : ITokenService
    {
        public TokenResult GetToken()
        {
            var token = new TokenFactory().GetToken("valor subjectId", "valor issuer", "valor claims");

            ITokenStore tokenStore = new InMemoryTokenStore();
            tokenStore.SetToken(token);

            return new TokenResult
            {
                Token = token.Value,
                Expiration = token.CreationDate
            };
        }

        public TokenValidationResult ValidateToken(string token)
        {
            var result = new TokenValidationResult();
            if (string.IsNullOrEmpty(token))
            {
                return result;
            }

            var tokenValidation = new GuidTokenValidation(new InMemoryTokenStore());
            var tokenValidationResult = tokenValidation.Validate(token);
            result.Valid = tokenValidationResult.Valid;

            return result;
        }
    }
}
