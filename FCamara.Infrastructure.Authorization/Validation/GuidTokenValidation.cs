﻿using System;
using FCamara.Infrastructure.Authorization.Models;

namespace FCamara.Infrastructure.Authorization.Validation
{
    public class GuidTokenValidation
    {
        private readonly ITokenStore _tokenStore;

        public GuidTokenValidation(ITokenStore tokenStore)
        {
            this._tokenStore = tokenStore;
        }

        public GuidTokenValidationResult Validate(string tokenValue)
        {
            var result = new GuidTokenValidationResult();

            var token = (GuidToken) this._tokenStore.GetToken(tokenValue);

            if (token == null) return result;

            if (IsValidToken(token))
            {
                result.Valid = true;
                result.Token = token;
            }

            return result;
        }

        private static bool IsValidToken(GuidToken token)
        {
            return token.CreationDate.AddMinutes(1) >= DateTime.Now;
        }
    }
}