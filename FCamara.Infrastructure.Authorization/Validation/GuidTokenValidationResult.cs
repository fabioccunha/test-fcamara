﻿using FCamara.Infrastructure.Authorization.Models;

namespace FCamara.Infrastructure.Authorization.Validation
{
    public class GuidTokenValidationResult
    {
        public bool Valid { get; set; }

        public GuidToken Token { get; set; }
    }
}