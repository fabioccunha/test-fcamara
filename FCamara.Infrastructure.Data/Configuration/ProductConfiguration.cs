﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using FCamara.Domain.Model;

namespace FCamara.Infrastructure.Data.Configuration
{
    internal class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Name)
                .HasMaxLength(80)
                .IsRequired();

            Property(x => x.Description)
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Price)
                .HasPrecision(10, 2);
        }
    }
}