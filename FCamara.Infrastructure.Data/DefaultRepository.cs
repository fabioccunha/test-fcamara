﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCamara.Domain.Model;
using FCamara.Domain.Repository;

namespace FCamara.Infrastructure.Data
{
    public class DefaultRepository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : Entity
    {
        protected readonly ProductContext Context;

        public DefaultRepository()
        {
            this.Context = new ProductContext();
        }

        public TEntity GetById(int id)
        {
            return this.Context.Set<TEntity>().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return this.Context.Set<TEntity>().ToList();
        }

        public void Add(TEntity entity)
        {
            this.Context.Set<TEntity>().Add(entity);
            this.Context.SaveChanges();
        }

        public void Remove(TEntity entity)
        {
            this.Context.Set<TEntity>().Remove(entity);
            this.Context.SaveChanges();
        }

        public void Dispose()
        {
            this.Context.Dispose();
        }
    }
}
