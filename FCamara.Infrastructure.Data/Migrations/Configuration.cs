using FCamara.Domain.Model;

namespace FCamara.Infrastructure.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FCamara.Infrastructure.Data.ProductContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FCamara.Infrastructure.Data.ProductContext context)
        {
            context.Products.Add(new Product("Bola de Futebol", "Bola de Futebol Adidas", 99.99m));
            context.Products.Add(new Product("Iphone 5S", "Celular Iphone 5S Prateado", 1800.00m));
            context.Products.Add(new Product("Arroz Paris", "Arroz Integral Paris 500g", 10.58m));
            context.Products.Add(new Product("Orion", "Bolacha Orion 3 unidades", 8.59m));
            context.Products.Add(new Product("Nike Dry", "Camiseta Nike Dry Fit", 88.00m));

            context.SaveChanges();
        }
    }
}
