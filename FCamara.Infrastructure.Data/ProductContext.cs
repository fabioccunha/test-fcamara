﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FCamara.Domain.Model;
using FCamara.Infrastructure.Data.Configuration;

namespace FCamara.Infrastructure.Data
{
    public class ProductContext : DbContext
    {
        public ProductContext() : base("productsdb")
        {
            Database.SetInitializer<ProductContext>(new CreateDatabaseIfNotExists<ProductContext>());
        }

        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new ProductConfiguration());
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
