﻿using FCamara.Domain.Model;
using FCamara.Domain.Repository;

namespace FCamara.Infrastructure.Data.Repository
{
    public class ProductRepository : DefaultRepository<Product>, IProductRepository
    {
    }
}
