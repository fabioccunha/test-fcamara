﻿using FCamara.Application.Service;
using FCamara.Domain.Repository;
using FCamara.Infrastructure.Authorization;
using FCamara.Infrastructure.Authorization.Models;
using FCamara.Infrastructure.Data;
using FCamara.Infrastructure.Data.Repository;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace FCamara.Infrastructure.IoC
{
    public class UnityIoC
    {
        public static UnityContainer Init()
        {
            var container = new UnityContainer();

            container.RegisterType(typeof(IRepository<>), typeof(DefaultRepository<>));
            container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<ITokenService, TokenService>();
            container.RegisterType<ITokenStore, InMemoryTokenStore>();

            var locator = new UnityServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => locator);

            return container;
        }
    }
}