(function() {
    'use strict';

    angular
        .module('app.authorization')
        .directive('tokenExpiration', tokenExpiration);

    tokenExpiration.$inject = ['$interval'];
    function tokenExpiration($interval) {
        
        var directive = {           
            link: link,
            restrict: 'A',
            scope: {
                date: '@'
            }
        };
        return directive;
        
        function link(scope, element, attrs) {
           
            var timeLeft, running = false;

            scope.$watch('date', function (v) {
                element.text("Token expira em 1 minuto");

                var future = new Date(scope.date);
                if(running) $interval.cancel(timeLeft);

                timeLeft = $interval(function () {
                    var diff = Math.ceil((future.getTime() - new Date().getTime()) / 1000);
                    running = true;
                    return element.text(format(diff));
                }, 1000);
            });

            element.on('$destroy', function() {
                $interval.cancel(timeLeft);
            });
        }

        function format(diff) {
            var value = diff % 60;
            var intro = "Token expira em ";
            var message;
            if(value > 1) {
                message = intro + value + " segundos";
            } else if(value == 1) {
                message = intro + value + " segundo";
            } else {
                message = "Token expirado!";
            }
            return message;
        }
    }
})();