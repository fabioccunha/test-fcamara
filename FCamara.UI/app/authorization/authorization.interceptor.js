﻿(function() {

    'use strict';

    angular
        .module('app.core')
        .factory('authorizationInterceptor', authorizationInterceptor);

    authorizationInterceptor.$inject = ['$q'];
    function authorizationInterceptor($q) {
        return {
            responseError: function (response) {
                if (response.status === 401) {
                    alert("Você não está autorizado.");
                }

                return $q.reject(response);
            }
        }
    }

})();