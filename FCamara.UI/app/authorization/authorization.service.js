(function() {
'use strict';

    angular
        .module('app.authorization')
        .factory('authorizationService', authorizationService);

    authorizationService.$inject = ['$http', 'api'];
    function authorizationService($http, api) {
        var service = {
            getToken: getToken
        };
        
        return service;

        ////////////////
        function getToken() { 
            return $http.get(api.URL + '/auth')
                .then(getTokenResult)
                .catch(getTokenFailed);

            function getTokenResult(response) {
                return response.data;
            }

            function getTokenFailed(error) {
                return console.error('XHR Failed for getToken ' + error);
            }
        }
    }
})();