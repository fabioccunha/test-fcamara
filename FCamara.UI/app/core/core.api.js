﻿(function () {

    'use strict';

    angular
        .module('app.core')
        .constant('api', {
            URL: 'http://localhost:3960/api'
        });
})();