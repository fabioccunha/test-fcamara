(function() {

'use strict';

    angular
        .module('app.product')
        .config(config);

    config.inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', 'authorizationInterceptor'];
    function config($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('products', {
                url: '/products',
                templateUrl: '/app/product/products.html',
                controller: 'ProductController as vm'
            });

        $urlRouterProvider.otherwise("/products");
        $httpProvider.interceptors.push('authorizationInterceptor');

    }
    
})();