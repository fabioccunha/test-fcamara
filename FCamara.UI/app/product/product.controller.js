(function() {

'use strict';

    angular
        .module('app.product')
        .controller('ProductController', ProductController);

    ProductController.$inject = ['$window', 'productService', 'authorizationService'];
    function ProductController($window, productService, authorizationService) {
        var vm = this;

        vm.getToken = getToken;
        vm.getProducts = getProducts;

        function getToken() {
            authorizationService.getToken().then(function(result) {
                vm.token = result.token;
                vm.expiration = result.expiration;
                
                $window.localStorage.setItem('token', result.token);
            });
            
        }

        function getProducts() {
            productService.getProducts().then(function (result) {
                vm.products = result;
            });
        }
    }
})();