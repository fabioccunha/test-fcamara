(function() {

'use strict';

    angular
        .module('app.product')
        .factory('productService', productService);

    productService.$inject = ['$window', '$http', 'api'];
    function productService($window, $http, api) {

        var service = {
            getProducts: getProducts
        };
        
        return service;
        ////////

        function getProducts() {

            var token = $window.localStorage.getItem('token');

            return $http.get(api.URL + '/product', {
                    headers: {
                        'Authorization': 'guid ' + token
                    }
                })
                .then(getProductsResult)
                .catch(getProductsFailed);

            function getProductsResult(response) {
                return response.data;
            }

            function getProductsFailed(error) {
                return console.error('XHR Failed for getProducts ' + error);
            }
        }
    }

})();