﻿using Fcamara.Api.Controllers;
using FCamara.Application.Service;
using FCamara.Domain.Model;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fcamara.Api.Tests
{
    [TestFixture]
    public class ProductControllerTest
    {
        private List<Product> _products;
        private Mock<IProductService> _mockProductService;
        private ProductController productController;

        [SetUp]
        public void Init()
        {
            this._products = new List<Product>
            {
                new Product("Bola", "Bola de Futebol", 99.99m),
                new Product("Chuteira", "Chuteira de Futebol", 250.00m)
            };

            this._mockProductService = new Mock<IProductService>();
            this._mockProductService.Setup(x => x.FindAllProducts()).Returns(_products);

            productController = new ProductController(this._mockProductService.Object);

            
        }

        [Test]
        public void Should_Return_A_Products_List()
        {
            var products =  productController.Get();

            Assert.IsNotNull(products);
            Assert.IsTrue(products.Any());
            Assert.AreEqual(2, products.Count());

            this._mockProductService.Verify(mock => mock.FindAllProducts(), Times.Once());
        }
    }
}
