using Microsoft.Practices.Unity;
using System.Web.Http;
using FCamara.Infrastructure.IoC;
using Microsoft.Practices.ServiceLocation;
using Unity.WebApi;

namespace Fcamara.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = UnityIoC.Init();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}