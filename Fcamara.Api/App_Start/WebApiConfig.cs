﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using Fcamara.Api.Infrastructure;
using FCamara.Infrastructure.Authorization;
using FCamara.Infrastructure.IoC;
using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json.Serialization;

namespace Fcamara.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {   
            
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(
                new MediaTypeHeaderValue("application/json-patch+json"));
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var cors = new System.Web.Http.Cors.EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            UnityConfig.RegisterComponents();

            ITokenService tokenService = ServiceLocator.Current.GetInstance<ITokenService>();
            config.Filters.Add(new ApiAuthorizeFilter(tokenService));
            
        }
    }
}
