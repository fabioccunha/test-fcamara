﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Fcamara.Api.Models;
using FCamara.Infrastructure.Authorization;
using FCamara.Infrastructure.Authorization.Dto;

namespace Fcamara.Api.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthorizationController : ApiController
    {
        private readonly ITokenService _tokenService;

        public AuthorizationController(ITokenService tokenService)
        {
            this._tokenService = tokenService;
        }

        [Route("")]
        [HttpGet]
        public TokenViewModel Authorize()
        {
            var tokenResult = this._tokenService.GetToken();
            return new TokenViewModel
            {
                Token = tokenResult.Token,
                Expiration = tokenResult.Expiration
            };
        }

        [Route("validate/{token}")]
        [HttpGet]
        public bool ValidateToken(string token)
        {
            var validationResult = this._tokenService.ValidateToken(token);
            return validationResult.Valid;
        }
    }
}
