﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Fcamara.Api.Infrastructure;
using FCamara.Application.Service;
using FCamara.Domain.Model;

namespace Fcamara.Api.Controllers
{
    public class ProductController : ApiController
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            this._productService = productService;
        }

        [ApiAuthorize]
        public IEnumerable<Product> Get()
        {
            return this._productService.FindAllProducts();
        } 
    }
}
