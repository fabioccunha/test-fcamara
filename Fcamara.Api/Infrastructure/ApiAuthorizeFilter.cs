﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FCamara.Infrastructure.Authorization;
using FCamara.Infrastructure.Authorization.Models;

namespace Fcamara.Api.Infrastructure
{
    public class ApiAuthorizeFilter : ActionFilterAttribute
    {
        private readonly ITokenService _tokenService;

        public ApiAuthorizeFilter(ITokenService tokenService)
        {
            this._tokenService = tokenService;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<ApiAuthorizeAttribute>().Any() ||
                actionContext.ActionDescriptor.GetCustomAttributes<ApiAuthorizeAttribute>().Any())
            {

                var authorizationHeader = actionContext.Request.Headers.GetValues("Authorization");

                if (authorizationHeader != null)
                {
                    string token = formatValidToken(Convert.ToString(authorizationHeader.FirstOrDefault()));
                    var validationResult = this._tokenService.ValidateToken(token);
                    if (!validationResult.Valid)
                    {
                        HttpContext.Current.Response.AddHeader("Authorization", token);
                        HttpContext.Current.Response.AddHeader("AuthorizationStatus", "NotAuthorized");
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                        return;
                    }

                    HttpContext.Current.Response.AddHeader("Authorization", token);
                    HttpContext.Current.Response.AddHeader("AuthorizationStatus", "Authorized");
                    return;
                }
            }
        }

        private string formatValidToken(string tokenHeader)
        {
            return tokenHeader.Replace("guid ", "");
        }

    }
}